package com.treskie.conrad.qrcontact;

import android.content.Intent;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

public class ReceiveContactInformation extends AppCompatActivity {
    private IntentIntegrator qrScan;
    private TextView contactName;
    private TextView contactNumber;

    private static final String TAG = "ReceiveContactInfo";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receive_contact_information);
        qrScan = new IntentIntegrator(this);
        contactName = findViewById(R.id.ContactName);
        contactNumber = findViewById(R.id.ContactNumber);
    }

    public void startScanActivity(View view){
        qrScan.initiateScan();
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode,resultCode,data);
        if(result == null) {
            Log.d(TAG, "IntentResult is null.");
        } else {
            try {
                ContactInformation receivedContactInformation = new ContactInformation();
                receivedContactInformation = receivedContactInformation.toContactInformation(result.getContents(),"&");
                contactName.setText("Name: "+receivedContactInformation.Name);
                contactNumber.setText("Contact Number: "+receivedContactInformation.ContactNumber);
                Toast.makeText(this, "Scan complete!", Toast.LENGTH_LONG).show();

                Intent contactIntent = new Intent(ContactsContract.Intents.Insert.ACTION);
                contactIntent.setType(ContactsContract.RawContacts.CONTENT_TYPE);

                contactIntent
                        .putExtra(ContactsContract.Intents.Insert.NAME, receivedContactInformation.Name)
                        .putExtra(ContactsContract.Intents.Insert.PHONE, receivedContactInformation.ContactNumber);

                startActivityForResult(contactIntent, 1);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}
