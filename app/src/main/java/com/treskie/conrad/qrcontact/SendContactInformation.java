package com.treskie.conrad.qrcontact;

import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class SendContactInformation extends AppCompatActivity {

    private ListView contactsListView;
    private EditText searchText;
    private HashMap<String, String> contactInformationMap = new HashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_contact_information);
        contactsListView = findViewById(R.id.ContactsListView);
        searchText = findViewById(R.id.SearchText);
        loadContacts();

    }

    public void loadContacts(){
        ContentResolver contentResolver = getContentResolver();
        Cursor cursor = contentResolver.query(ContactsContract.Contacts.CONTENT_URI,null,null,null,null);

        if (cursor.getCount() > 0) {
            while (cursor.moveToNext()){
                ContactInformation contactInformation = new ContactInformation();
                String id = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
                String name = cursor.getString((cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME)));
                int hasPhoneNumber = Integer.parseInt((cursor.getString(cursor.getColumnIndex((ContactsContract.Contacts.HAS_PHONE_NUMBER)))));

                if (hasPhoneNumber > 0){
                    Cursor contentCursor = contentResolver.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,null,
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?", new String[]{id},null);

                    while (contentCursor.moveToNext()){
                        contactInformation.Name = name;
                        contactInformation.ContactNumber = contentCursor.getString(contentCursor.getColumnIndex((ContactsContract.CommonDataKinds.Phone.NUMBER)));
                        contactInformationMap.put(contactInformation.Name, contactInformation.ContactNumber);
                    }
                    contentCursor.close();
                }
            }
            setAdapter();
        }
        cursor.close();
    }

    public void sendToViewer(ContactInformation contactInformation){
        Intent viewQRCode = new Intent(this, QRCodeViewer.class);
        viewQRCode.putExtra("ContactInfo", contactInformation);
        startActivity(viewQRCode);
    }

    public void setAdapter(){
        List<HashMap<String, String>> inputMapList = new ArrayList<>();

        final SimpleAdapter adapter = new SimpleAdapter(this,inputMapList, R.layout.activity_send_contact_information_item,
                new String[] {"Name", "Contact Number"},
                new int[]{R.id.ListItemContactName, R.id.ListItemContactNumber});

        Iterator it = contactInformationMap.entrySet().iterator();

        while(it.hasNext()){
            HashMap<String, String> resultsMap = new HashMap<>();
            Map.Entry pair = (Map.Entry)it.next();
            resultsMap.put("Name", pair.getKey().toString());
            resultsMap.put("Contact Number",pair.getValue().toString());
            inputMapList.add(resultsMap);
        }

        contactsListView.setAdapter(adapter);

        //Setting up the listeners.
        contactsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                HashMap<String, String> selectedMap = (HashMap<String, String>) adapter.getItem(i);
                ContactInformation selectedContactInformation = new ContactInformation();
                selectedContactInformation.Name = selectedMap.get("Name").toString();
                selectedContactInformation.ContactNumber = selectedMap.get("Contact Number").toString();

                sendToViewer(selectedContactInformation);
            }
        });

        searchText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                adapter.getFilter().filter(charSequence);
            }

            @Override
            public void afterTextChanged(Editable editable) {
                editable.getFilters();
            }
        });
    }

    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main,menu);
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()){
            case R.id.action_receiveContactInformation:
                Intent sendContactInfoPage = new Intent(this, ReceiveContactInformation.class);
                startActivity(sendContactInfoPage);
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
