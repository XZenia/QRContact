package com.treskie.conrad.qrcontact;

import java.io.Serializable;

public class ContactInformation implements Serializable{

    public String Name;

    public String ContactNumber;

    public ContactInformation toContactInformation(String input, String symbol){
        String[] splittedInput = input.split(symbol);
        ContactInformation newContactInformation = new ContactInformation();
        newContactInformation.Name = splittedInput[0];
        newContactInformation.ContactNumber = splittedInput[1];
        return newContactInformation;
    }
}
